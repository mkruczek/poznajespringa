package pl.sdacademy.poznajespringa.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sdacademy.poznajespringa.dto.HelloWorldDto;

import java.util.Date;

/**
 * Created by RENT on 2017-06-27.
 */

@RestController
@RequestMapping("/")
public class ListController {

    @RequestMapping("/test")
    public HelloWorldDto[] returnSingleName(){
        HelloWorldDto helloWorldDto = new HelloWorldDto();
        helloWorldDto.setAge(12);
        helloWorldDto.setDate(new Date());
        helloWorldDto.setName("Janek");


        HelloWorldDto helloWorldDto1 = new HelloWorldDto();
        helloWorldDto1.setAge(232);
        helloWorldDto1.setDate(new Date());
        helloWorldDto1.setName("Stefan");

        HelloWorldDto helloWorldDto2 = new HelloWorldDto();
        helloWorldDto2.setAge(2);
        helloWorldDto2.setDate(new Date());
        helloWorldDto2.setName("Kamil");

        HelloWorldDto[] lista = {helloWorldDto, helloWorldDto1, helloWorldDto2};

        return lista;
    }

}
