package pl.sdacademy.poznajespringa.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.sdacademy.poznajespringa.dto.ClientDto;
import pl.sdacademy.poznajespringa.dto.CommonDto;
import pl.sdacademy.poznajespringa.dto.FlatDto;
import pl.sdacademy.poznajespringa.model.Client;
import pl.sdacademy.poznajespringa.model.Flat;
import pl.sdacademy.poznajespringa.repository.ClientReposytory;
import pl.sdacademy.poznajespringa.repository.FlatRepository;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RENT on 2017-06-29.
 */

@RestController
public class CommonDtoController {


    @Autowired
    public ClientReposytory clientReposytory;

    @Autowired
    private FlatRepository flatRepository;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String uploadDataFlat(ModelMap modelMap, @RequestBody CommonDto commonDto) {

        Client client = new Client();
        client.setId(commonDto.getIdClient());
        client.setName(commonDto.getName());
        client.setHajs(commonDto.getHajs());
        client.setPhoneNumber(commonDto.getPhoneNumber());
        client.setAdded(convertStringToTimestamp(commonDto.getAddedClient()));
        client.setModified(convertStringToTimestamp(commonDto.getModifiedClient()));
        clientReposytory.save(client);

        Flat flat = new Flat();
        flat.setId(commonDto.getIdFlat());
        flat.setArea(commonDto.getArea());
        flat.setPrice(commonDto.getPrice());
        flat.setRooms(commonDto.getRooms());
        flat.setAdded(convertStringToTimestamp(commonDto.getAddedFlat()));
        flat.setModified(convertStringToTimestamp(commonDto.getModifiedFlat()));
        flatRepository.save(flat);

        return "Succes";
    }

    public static Timestamp convertStringToTimestamp(String str_date) {
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(str_date);
            java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

            return timeStampDate;
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return null;
        }
    }

}
