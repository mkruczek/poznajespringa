package pl.sdacademy.poznajespringa.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.poznajespringa.dto.FlatDto;
import pl.sdacademy.poznajespringa.model.Flat;
import pl.sdacademy.poznajespringa.repository.FlatRepository;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by RENT on 2017-06-28.
 */

@RestController
public class FlatController {

    @Autowired
    private FlatRepository flatRepository;

    @RequestMapping("/allFlats")
    public List<FlatDto> allFlats (){

        List<Flat> flatList = flatRepository.findAll();
        List<FlatDto> flatDtoList = new ArrayList<>();

        for (Flat flat : flatList){
            FlatDto flatDto = new FlatDto();
            flatDto.setId(flat.getId());
            flatDto.setArea(flat.getArea());
            flatDto.setRooms(flat.getRooms());
            flatDto.setPrice(flat.getPrice());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd");
            flatDto.setAdded(simpleDateFormat.format(flat.getAdded()));
            flatDto.setModified(simpleDateFormat.format(flat.getModified()));
            flatDtoList.add(flatDto);
        }
     return flatDtoList;
    }

    @RequestMapping("/flat/{id}")
    public FlatDto getSingleFlat (@PathVariable("id") Integer id){

        Flat flat = flatRepository.findOne(id);
        FlatDto flatDto = new FlatDto();

        flatDto.setId(flat.getId());
        flatDto.setRooms(flat.getRooms());
        flatDto.setArea(flat.getArea());
        flatDto.setPrice(flat.getPrice());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd");
        flatDto.setAdded(simpleDateFormat.format(flat.getAdded()));
        flatDto.setModified(simpleDateFormat.format(flat.getModified()));

        return flatDto;
    }

    @RequestMapping(value = "/flat/add", method = RequestMethod.POST)
    public String uploadDataFlat(ModelMap modelMap, @RequestBody FlatDto flatDto) throws ParseException {
        Flat flat = new Flat();

        flat.setId(flatDto.getId());
        flat.setArea(flatDto.getArea());
        flat.setPrice(flatDto.getPrice());
        flat.setRooms(flatDto.getRooms());

        flat.setAdded(convertStringToTimestamp(flatDto.getAdded()));
        flat.setModified(convertStringToTimestamp(flatDto.getModified()));
        flatRepository.save(flat);

        return "Succes";
    }

    public static Timestamp convertStringToTimestamp(String str_date) {
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(str_date);
            java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

            return timeStampDate;
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return null;
        }
    }
}
