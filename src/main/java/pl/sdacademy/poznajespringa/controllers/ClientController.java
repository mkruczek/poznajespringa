package pl.sdacademy.poznajespringa.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.poznajespringa.dto.ClientDto;
import pl.sdacademy.poznajespringa.dto.FlatDto;
import pl.sdacademy.poznajespringa.model.Client;
import pl.sdacademy.poznajespringa.model.Flat;
import pl.sdacademy.poznajespringa.repository.ClientReposytory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by RENT on 2017-06-28.
 */

@RestController
public class ClientController {

    @Autowired
    public ClientReposytory clientReposytory;

    @RequestMapping("/allClients")
    public List<ClientDto> allClients() {
        List<Client> clientList = clientReposytory.findAll();
        List<ClientDto> clientDtoList = new ArrayList<>();

        for (Client client : clientList) {
            ClientDto clientDto = new ClientDto();
            clientDto.setId(client.getId());
            clientDto.setHajs(client.getHajs());
            clientDto.setName(client.getName());
            clientDto.setPhoneNumber(client.getPhoneNumber());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            clientDto.setAdded(simpleDateFormat.format(client.getAdded()));
            clientDto.setModified(simpleDateFormat.format(client.getModified()));
            clientDtoList.add(clientDto);
        }
        return clientDtoList;
    }

    @RequestMapping("/client/{id}")
    public ClientDto getSingleClient(@PathVariable("id") Integer id) {

        Client client = clientReposytory.findOne(id);

        ClientDto clientDto = new ClientDto();
        clientDto.setId(client.getId());
        clientDto.setHajs(client.getHajs());
        clientDto.setName(client.getName());
        clientDto.setPhoneNumber(client.getPhoneNumber());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        clientDto.setAdded(simpleDateFormat.format(client.getAdded()));
        clientDto.setModified(simpleDateFormat.format(client.getModified()));

        return clientDto;
    }

    @RequestMapping(value = "/client/add", method = RequestMethod.POST)
    public String uploadData(ModelMap modelMap, @RequestBody ClientDto clientDto) throws ParseException {
        Client client = new Client();

        client.setId(clientDto.getId());
        client.setName(clientDto.getName());
        client.setHajs(clientDto.getHajs());
        client.setPhoneNumber(clientDto.getPhoneNumber());
        client.setAdded(convertStringToTimestamp(clientDto.getAdded()));
        client.setModified(convertStringToTimestamp(clientDto.getModified()));


        clientReposytory.save(client);

        return "Succes";
    }

    public static Timestamp convertStringToTimestamp(String str_date) {
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(str_date);
            java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

            return timeStampDate;
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return null;
        }

    }
}
