package pl.sdacademy.poznajespringa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.poznajespringa.model.Flat;

/**
 * Created by RENT on 2017-06-28.
 */
public interface FlatRepository extends JpaRepository<Flat, Integer> {
}
