package pl.sdacademy.poznajespringa.dto;

import java.util.Date;

/**
 * Created by RENT on 2017-06-27.
 */
public class HelloWorldDto {

    private Date date;
    private String name;
    private Integer age;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
