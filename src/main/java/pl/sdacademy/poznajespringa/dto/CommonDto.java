package pl.sdacademy.poznajespringa.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.sdacademy.poznajespringa.model.Flat;
import pl.sdacademy.poznajespringa.repository.ClientReposytory;
import pl.sdacademy.poznajespringa.repository.FlatRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RENT on 2017-06-29.
 */
public class CommonDto {

    //client
    private Integer idClient;
    private String name;
    private BigDecimal hajs;
    private String phoneNumber;
    private String addedClient;
    private String modifiedClient;

    //flat
    private Integer idFlat;
    private Integer rooms;
    private BigDecimal area;
    private BigDecimal price;
    private String addedFlat;
    private String modifiedFlat;

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getHajs() {
        return hajs;
    }

    public void setHajs(BigDecimal hajs) {
        this.hajs = hajs;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddedClient() {
        return addedClient;
    }

    public void setAddedClient(String addedClient) {
        this.addedClient = addedClient;
    }

    public String getModifiedClient() {
        return modifiedClient;
    }

    public void setModifiedClient(String modifiedClient) {
        this.modifiedClient = modifiedClient;
    }

    public Integer getIdFlat() {
        return idFlat;
    }

    public void setIdFlat(Integer idFlat) {
        this.idFlat = idFlat;
    }

    public Integer getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getAddedFlat() {
        return addedFlat;
    }

    public void setAddedFlat(String addedFlat) {
        this.addedFlat = addedFlat;
    }

    public String getModifiedFlat() {
        return modifiedFlat;
    }

    public void setModifiedFlat(String modifiedFlat) {
        this.modifiedFlat = modifiedFlat;
    }
}
