package pl.sdacademy.poznajespringa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoznajespringaApplication {

	public static void main(String[] args) {

		SpringApplication.run(PoznajespringaApplication.class, args);
	}
}
